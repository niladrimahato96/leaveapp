import React from 'react';
import Aux from '../../../hoc/Auxi';
import avatar from '../../../assets/images/faces/male/41.jpg';
import Tabs from 'react-responsive-tabs';
import './tabStyle.css';
import CalenderMaterial from '../CalenderMaterial/CalenderMaterial';
import Balloon from '../Balloon/Balloon';


class Home extends React.Component {
  state = {
    display : true,
    accordionValue : 'heading0'
  }
  toggleAccordionHandler = (obj) => {
    this.setState({
      display : true,
      accordionValue : obj
    });
  }

  render(){


    const allData = {
        "code": 200,
        "message": "Success",
        "empID": "271",
        "empCode": 180,
        "leaveBalanace": 8,
        "IsPL": "False",
        "data": [
            {
                "day": "Saturday",
                "date": "2018-12-29T00:00:00+00:00",
                "employeeName": [
                    "Ankit Katiyar",
                    "Biswajit Sur",
                    "Deepu Shivhare",
                    "Ganesh Paul",
                    "Jasmeet Singh Saran"
                ]
            },
            {
                "day": "Sunday",
                "date": "2018-12-30T00:00:00+00:00",
                "employeeName": [
                  "Biswajit Sur",
                  "Deepu Shivhare"
                ]
            },
            {
                "day": "Monday",
                "date": "2018-12-31T00:00:00+00:00",
                "employeeName": [
                    "Ganesh Paul",
                    "Jasmeet Singh Saran",
                    "Palak Gupta",
                    "Sanghamitra Roy"
                ]
            },
            {
                "day": "Tuesday",
                "date": "2019-01-01T00:00:00+00:00",
                "employeeName": [
                    "Ganesh Paul",
                    "Isha Koul",
                    "Jasmeet Singh Saran",
                    "Palak Gupta"
                ]
            },
            {
                "day": "Wednesday",
                "date": "2019-01-02T00:00:00+00:00",
                "employeeName": [
                    "Ganesh Paul",
                    "Isha Koul",
                    "Jasmeet Singh Saran",
                    "Palak Gupta"
                ]
            },
            {
                "day": "Thursday",
                "date": "2019-01-03T00:00:00+00:00",
                "employeeName": [
                  "Palak Gupta",
                  "Shivam Pathak"
                ]
            },
            {
                "day": "Friday",
                "date": "2019-01-04T00:00:00+00:00",
                "employeeName": [
                  "Palak Gupta",
                  "Shivam Pathak"
                ]
            },
            {
                "day": "Saturday",
                "date": "2019-01-05T00:00:00+00:00",
                "employeeName": [
                    "Biswajit Sur",
                    "Chandan Singh",
                    "Jasmeet Singh Saran",
                    "Nitin Rawal",
                    "Palak Gupta",
                    "Shivam Pathak"
                ]
            },
            {
                "day": "Sunday",
                "date": "2019-01-06T00:00:00+00:00",
                "employeeName": []
            },
            {
                "day": "Monday",
                "date": "2019-01-07T00:00:00+00:00",
                "employeeName": []
            },
            {
                "day": "Tuesday",
                "date": "2019-01-08T00:00:00+00:00",
                "employeeName": []
            },
            {
                "day": "Wednesday",
                "date": "2019-01-09T00:00:00+00:00",
                "employeeName": []
            },
            {
                "day": "Thursday",
                "date": "2019-01-10T00:00:00+00:00",
                "employeeName": [
                  "Palak Gupta",
                  "Shivam Pathak"
                ]
            },
            {
                "day": "Friday",
                "date": "2019-01-11T00:00:00+00:00",
                "employeeName": [
                  "Palak Gupta",
                  "Shivam Pathak"
                ]
            },
            {
                "day": "Saturday",
                "date": "2019-01-12T00:00:00+00:00",
                "employeeName": [
                  "Palak Gupta",
                  "Shivam Pathak"
                ]
            }
        ]
    }
    function getTabs() {
      return allData.data.reduce((accum, alldata, index) => {
        if(alldata.day !== 'Saturday' && alldata.day !== 'Sunday'){
          let employees = '';


          if(alldata.employeeName !== null){
            employees =  alldata.employeeName.map((employee)=> {
              let profiles = [
                  'Associate Software Engineer',
                  'Software Developer',
                  'Account Manager',
                  'HR', 'IOS Developer',
                  'Android Developer',
                  'Web Developer',
                  'Designer',
                  'Release Manager'
                ];
              let randomItem = profiles[Math.floor(Math.random()*profiles.length)];
              return(
                <tr>
                  <td className="w-1">
                    <span className="avatar" style={{ backgroundImage: `url(${avatar})` }}></span>
                  </td>
                  <td>
                    {employee}
                  </td>
                  <td>
                    {randomItem}
                  </td>
                  <td>
                    {Math.floor(Math.random() * (5 - 1 + 1)) + 1}
                  </td>
                </tr>
              );
            });
          }


          let day = alldata.date.substr(8,2);
          let month = alldata.date.substr(5,2);
          switch(month){
            case '01':
              month = 'Jan'
            break;
            case '02':
              month = 'Feb'
            break;
            case '03':
              month = 'Mar'
            break;
            case '04':
              month = 'Apr'
            break;
            case '05':
              month = 'May'
            break;
            case '06':
              month = 'Jun'
            break;
            case '07':
              month = 'Jul'
            break;
            case '08':
              month = 'Aug'
            break;
            case '09':
              month = 'Sep'
            break;
            case '10':
              month = 'Oct'
            break;
            case '11':
              month = 'Nov'
            break;
            case '12':
              month = 'Dec'
            break;
            default:
            break;
          }
          let value = {

            showInkBar: true,
          title: day+" "+month,
          selectedTabKey: '2',
          getContent: () => {

            if(alldata.date.substr(0,10) === "2019-01-01"){
              return (
                <div class=" col-lg-12 col-md-12 col-sm-6" style={{height:'320px'}}>
                    <div class="card">
                      <div class="card-body text-center">
                        <div class="h5 text-green">Happy New Year</div>
                        <div class="display-4 font-weight-bold mb-4">2019</div>
                      </div>
                    </div>
                  </div>
              );
            }

            if(alldata.employeeName.length === 0){
              return (
                <div class=" col-lg-12 col-md-12 col-sm-6" style={{height:'320px'}}>
                    <div class="card">
                      <div class="card-body text-center">
                        <div class="display-4 font-weight-bold mb-4 text-green"><i className="fe fe-users"></i></div>
                        <div class="display-4 font-weight-bold mb-4">House Full</div>
                      </div>
                    </div>
                  </div>
              );
            }

            return(
            <div className="table-responsive" style={{height:'320px'}}>
              <table className="table card-table table-striped table-vcenter">
                <thead>
                  <th colspan="2">User</th>
                  <th>Profile</th>
                  <th>No. of Days</th>
                </thead>
                <tbody>
                  {employees}
                </tbody>
              </table>
            </div>
          );
          },
          /* Optional parameters */
          key: index,
          tabClassName: 'tab',
          panelClassName: 'panel',
          removeActiveOnly : true
        }
          accum.push(value);
        }
        return accum;
        },[]);
    }

        let tab = <Tabs items={getTabs()} showMore='true' showMoreLabel="..." transformWidth="200"/>;


        return (
            <Aux>

            <div class="row row-cards">
              <div class="col-lg-6 col-sm-4">
                <div className="row row-cards">
                <div className="col-6 col-sm-4 col-lg-4">
                  <div class="card">
                    <div class="card-body p-3 text-center">
                      <div class="text-right text-green">

                        <i class="fe fe-trending-up"></i>
                      </div>
                      <div class="h1 m-0">12</div>
                      <div class="text-muted mb-4">Earned Leaves</div>
                    </div>
                  </div>
                </div>
                <div className="col-6 col-sm-4 col-lg-4">
                  <div class="card">
                    <div class="card-body p-3 text-center">
                      <div class="text-right text-red">

                        <i class="fe fe-activity"></i>
                      </div>
                      <div class="h1 m-0">6</div>
                      <div class="text-muted mb-4">Compensatory Leaves</div>
                    </div>
                  </div>
                </div>
                <div className="col-6 col-sm-4 col-lg-4">
                  <div class="card">
                    <div class="card-body p-3 text-center">
                      <div class="text-right text-red">

                        <i class="fe fe-trending-down"></i>
                      </div>
                      <div class="h1 m-0">2</div>
                      <div class="text-muted mb-4">B'day/Anniv. Leaves</div>
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-4 col-lg-12">


                      <h3 className="card-title"><i class="fa fa-birthday-cake text-muted"></i>&nbsp;Birthdays</h3>

                    <div className=" p-3 text-center" style={{height: '275px'}}>
                        {/*<CalenderMaterial />*/}
                        <div className="row row-cards">
                          <Balloon animate="animateBalloon2" color="purple" name="Niladri Mahato" bdate="01 Jan"/>
                          <Balloon animate="animateBalloon1" color="yellow" name="Amit" bdate="05 Jan"/>
                          <Balloon  animate="animateBalloon2" color="red" name="Sachin" bdate="08 Jan"/>
                          <Balloon  animate="animateBalloon3" color="yellow" name="Rohit" bdate="08 Jan"/>
                          <Balloon  animate="animateBalloon1" color="blue" name="Deepak" bdate="12 Jan"/>
                          <Balloon  animate="animateBalloon3" color="green" name="Vipin" bdate="14 Jan"/>
                          <Balloon  animate="animateBalloon2" color="red" name="Sonia" bdate="20 Jan"/>
                          <Balloon  animate="animateBalloon3" color="blue" name="Aman" bdate="22 Jan"/>
                          <Balloon  animate="animateBalloon1" color="purple" name="Priyanka" bdate="22 Jan"/>
                          <Balloon  animate="animateBalloon2" color="green" name="Abhijit" bdate="30 Jan"/>
                        </div>
                    </div>

                </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="card">
                  <div className="card-header">
                    <h3 className="card-title">Employees on Leave</h3>
                  </div>
                  <div className="card-body p-3 text-center">
                      {tab}
                  </div>
                </div>
              </div>



            </div>


                </Aux>
        );
  }
}

export default Home;
