import React from 'react';
import BigCalender from 'react-big-calendar';
import moment from 'moment';
import './react-big-calendar.css';



class CalenderMaterial extends React.Component{
  render(){
    const localizer = BigCalender.momentLocalizer(moment) ;
    const events = [
    {
        start: '2019-01-04',
        end: '2019-01-05',
        eventClasses: 'Event 1',
        title: 'Niladri Mahato',
        description: 'This is a test description of an event',
    },
    {
        start: '2019-01-04',
        end: '2019-01-05',
        eventClasses: 'Event 2',
        title: 'Amit Jha',
        description: 'This is a test description of an event',
    },
    {
        start: '2019-00-08',
        end: '2019-00-10',
        title: 'test event',
        description: 'This is a test description of an event',
        data: 'you can add what ever random data you may want to use later',
    },
];
    return(
      <BigCalender
      events={events}
    step={60}
    showMultiDayTimes
    defaultDate={new Date(2019, 0, 2)}
    localizer={localizer}
    views={['month','day']}
      />
    );
  }
}

export default CalenderMaterial;
