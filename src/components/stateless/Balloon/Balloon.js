import React from 'react';
import './Balloon.css';
import avatar from '../../../assets/images/faces/male/41.jpg';
import Radium from 'radium';

class Balloon extends React.Component{

  render(){

    return(
      <div className={`balloonContainerMain ${this.props.animate}`}>
        <div className="balloonContainer">
          <div className={`balloon ${this.props.color}`}>
            <img src={avatar} alt="fsd" style={{borderRadius:'50%', marginTop: '10px'}}/>
          </div>
          <span className={`before ${this.props.color}`}>▲</span>
          <div className="balloonN">
            {this.props.name} <br />
            {this.props.bdate}
          </div>
        </div>
      </div>
    );
  }
}

export default Radium(Balloon);
